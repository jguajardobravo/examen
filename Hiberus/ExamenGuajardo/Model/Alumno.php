<?php


namespace Hiberus\ExamenGuajardo\Model;

use Hiberus\ExamenGuajardo\Api\Data\AlumnoInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Alumno
 * @package Hiberus\ExamenGuajardo\Model
 */
class Alumno extends AbstractModel implements AlumnoInterface
{

    protected function _construct()
    {
        $this->_init(\Hiberus\ExamenGuajardo\Model\ResourceModel\Alumno::class);
    }

    /**
     * @return int|mixed
     */
    public function getId()
    {
        return $this->_getData(self::ID);
    }

    /**
     * @return mixed|string
     */
    public function getFirstName()
    {
        return $this->_getData(self::FIRSTNAME);
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->_getData(self::LASTNAME);
    }

    /**
     * @param int|mixed $id
     * @return AlumnoInterface|Alumno|AbstractModel
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @param string $firstname
     * @return AlumnoInterface|Alumno
     */
    public function setFirstName($firstname)
    {
        return $this->setData(self::FIRSTNAME, $firstname);
    }

    /**
     * @param string $lastName
     * @return AlumnoInterface|Alumno
     */
    public function setLastName($lastName)
    {
        return $this->setData(self::LASTNAME, $lastName);
    }

    /**
     * @return string
     */
    public function getMark()
    {
        return $this->_getData(self::MARK);
    }

    /**
     * @param string $mark
     * @return AlumnoInterface
     */
    public function setMark($mark)
    {
        return $this->setData(self::MARK, $mark);
    }

    
}
