<?php
/**
 * @author: daniDLL
 * Date: 18/11/20
 * Time: 20:39
 */

namespace Hiberus\ExamenGuajardo\Model;

use Hiberus\ExamenGuajardo\Api\Data\AlumnoSearchResultsInterface;
use Magento\Framework\Api\SearchResults;

/**
 * Class AlumnoSearchResults
 * @package Hiberus\ExamenGuajardo\Model
 */
class AlumnoSearchResults extends SearchResults implements AlumnoSearchResultsInterface
{
}
