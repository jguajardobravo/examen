define(['jquery'], function($){
    return function(config, element){
        $( ".notasbutton" ).on('click', function() {
            console.log("he clickado");
            if ( $(element).css('visibility') == 'visible' ){
                $(element).css('visibility', 'hidden');
                document.getElementById("textobutton").innerHTML = "Mostrar Notas";
                $(".notasbutton").css('background-color','green');
            }
            else{
                $(element).css('visibility','visible');
                document.getElementById("textobutton").innerHTML = "Ocultar Notas";
                $(".notasbutton").css('background-color','red');
            }
        })
    }
});