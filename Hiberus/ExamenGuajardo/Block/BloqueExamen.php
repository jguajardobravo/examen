<?php
namespace Hiberus\ExamenGuajardo\Block;
use Hiberus\ExamenGuajardo\Api\AlumnoRepositoryInterface;
use Magento\FrameWork\Api\SearchCriteriaBuilder;
use Magento\FrameWork\Api\SortOrderBuilder;
use Psr\Log\LoggerInterface;
class BloqueExamen extends \Magento\Framework\View\Element\Template
{
    protected $alumnoRepo;
    protected $critBuilder;
    protected $sortOrderBuilder;
    protected $logger;
    /**
     * BloqueExamen constructor.
     * @param LoggerInterface $logger
     * @param AlumnoRepository $alumnoRepo
     * @param SearchCriteriaBuilder $critBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     */
 public function __construct( \Psr\Log\LoggerInterface $logger, \Magento\Framework\View\Element\Template\Context $context, 
 \Hiberus\ExamenGuajardo\Api\AlumnoRepositoryInterface $alumnoRepo, \Magento\FrameWork\Api\SearchCriteriaBuilder $critBuilder, 
    \Magento\FrameWork\Api\SortOrderBuilder $sortOrderBuilder, array $data = [] ) {
        $this->sortOrderBuilder=$sortOrderBuilder;
        $this->critBuilder=$critBuilder;
        $this->alumnoRepo = $alumnoRepo;
        $this->logger=$logger;
        parent::__construct($context, $data);
    }
/**
 * Clase que lista alumnos 
 * 
 * @return array /app/code/Hiberus/ExamenGuajardo/Model/Alumno $alumno;
 */
    public function listAlumnos(){
        try{
            $misAlumnos=$this->alumnoRepo->getList(
            $this->critBuilder->create())->getItems();
            $this->logger->debug('Se han obtenido '.count($misAlumnos).'alumnos correctamente.');
        }catch(Exception $e){
            $this->logger->error('Error al acceder al repositorio de alumnos y usar criteria builder y sort order builder.');
        }
        
        return $misAlumnos;
    }
    /**
    * Clase que lista 3 mejores alumnos 
    * 
    * @return array /app/code/Hiberus/ExamenGuajardo/Model/Alumno $alumno;
    */
    public function get3notas(){
        return $this->alumnoRepo->getList(
            $this->critBuilder
            ->addSortOrder($this->sortOrderBuilder->setField('mark')->setDirection('DESC')->create())
            ->setPageSize(3)
            ->create()
        )->getItems();
    }
    /**
    * Clase que lista 3 mejores alumnos 
    * 
    * @return int $indiceMejorAlumno;
    */
    public function getValues(){
        try{
            $misAlumnos=$this->alumnoRepo->getList(
            $this->critBuilder->create())->getItems();
        }catch(Exception $e){
            $this->logger->error('Error al acceder al repositorio de alumnos y usar criteria builder y sort order builder.');
        }
        $mejorNota=0;
        $j=0;
        $sumaNotas=0;
        $indiceMejorNota=0;
        foreach($misAlumnos as $alumno){
            $miNota=$alumno->getMark();
            $sumaNotas+=$miNota;
            if($mejorNota<$miNota){
                $indiceMejorNota=$j;
                $mejorNota=$miNota;

            }   
            $j++;
        }
        $this->logger->debug('Media de notas de la clase '.$mediaNotas=$sumaNotas/count($misAlumnos));
        return [$indiceMejorNota,$mediaNotas];
        
    }

}
