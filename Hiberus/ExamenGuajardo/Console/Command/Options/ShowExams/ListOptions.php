<?php

namespace Hiberus\ExamenGuajardo\Console\Command\Options\ShowExams;

/**
 * Class ListOptions
 * @package Hiberus\ExamenGuajardo\Console\Command\Options\ShowExams
 */
class ListOptions
{
    /**
     * Show alumnos options list
     *
     * @return array
     */
    public function getOptionsList()
    {
        return $this->getBasicOptions();
    }

    /**
     * Basic options
     *
     * @return array
     */
    private function getBasicOptions()
    {
        return [

        ];
    }
}
