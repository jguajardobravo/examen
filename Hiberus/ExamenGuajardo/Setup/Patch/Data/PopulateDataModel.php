<?php

namespace Hiberus\ExamenGuajardo\Setup\Patch\Data;

use Hiberus\ExamenGuajardo\Api\Data\AlumnoInterface;
use Hiberus\ExamenGuajardo\Api\Data\AlumnoInterfaceFactory;

use Hiberus\ExamenGuajardo\Api\AlumnoRepositoryInterface;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\File\Csv;

/**
 * Class PopulateDataModel
 * @package Hiberus\ExamenGuajardo\Setup\Patch\Data
 */
class PopulateDataModel implements DataPatchInterface
{
    const FILE = 'app/code/Hiberus/ExamenGuajardo/Setup/data/import.csv';
    /**
     * @var AlumnoRepositoryInterface
     */
    private $alumnoRepository;

    /**
     * @var AlumnoInterfaceFactory
     */
    private $alumnoFactory;
    
    /**
     * @var Csv
     */
    private $csv;
    

    /**
     * PopulateDataModel constructor.
     * @param AlumnoRepositoryInterface $alumnoRepository
     * @param AlumnoInterfaceFactory $alumnoFactory
     * @param Cvs $csv
     */
    public function __construct(
        AlumnoRepositoryInterface $alumnoRepository,
        AlumnoInterfaceFactory $alumnoFactory,
        Csv $csv
    ) {
        $this->alumnoRepository = $alumnoRepository;
        $this->alumnoFactory = $alumnoFactory;
        $this->csv=$csv;
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->populateData();
    }

    /**
     * Populate Data Model
     */
    private function populateData()
    {
        $this->populateAlumnos();
    }    

    /**
     * Populate Alumnos Data
     */
    private function populateAlumnos()
    {
        $myData=$this->csv->getData(self::FILE);

            foreach($myData as $linea){
                /** @var AlumnoInterface $alumno */
                $alumno = $this->alumnoFactory->create();

                $alumno->setFirstName($linea[0])
                    ->setLastName($linea[1])
                    ->setMark(rand(0,1000)/100)
                ;

                $this->alumnoRepository->save(
                    $alumno
                );
           }
    }


    /**
     * @return string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }
}