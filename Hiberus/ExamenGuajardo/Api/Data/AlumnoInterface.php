<?php


namespace Hiberus\ExamenGuajardo\Api\Data;

/**
 * Interface AlumnoInterface
 * @package Hiberus\ExamenGuajardo\Api\Data
 */
interface AlumnoInterface
{
    const   TABLE   =   'hiberus_exam';

    const   ID      =   'id_exam';
    const   FIRSTNAME   =   'firstname';
    const   LASTNAME    =   'lastname';
    const   MARK  =   'mark';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return AlumnoInterface
     */
    public function setId($id_exam);

    /**
     * @param string $firstname
     * @return AlumnoInterface
     */
    public function setFirstName($firstname);

    /**
     * @return string
     */
    public function getFirstName();

    /**
     * @param string $lastName
     * @return AlumnoInterface
     */
    public function setLastName($lastname);

    /**
     * @return string
     */
    public function getLastName();

    /**
     * @return float
     */
    public function getMark();

    /**
     * @param float $mark
     * @return AlumnoInterface
     */
    public function setMark($mark);

}
