<?php


namespace Hiberus\ExamenGuajardo\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface AlumnoSearchResultsInterface
 * @package Hiberus\ExamenGuajardo\Api\Data
 */
interface AlumnoSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get alumno list.
     *
     * @return \Hiberus\ExamenGuajardo\Api\Data\AlumnoInterface[]
     */
    public function getItems();

    /**
     * Set alumno list.
     *
     * @param \Hiberus\ExamenGuajardo\Api\Data\AlumnoInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
