<?php

namespace Hiberus\ExamenGuajardo\Plugin;

use Hiberus\ExamenGuajardo\Api\Data\AlumnoInterface;
use Hiberus\ExamenGuajardo\Api\Data\AlumnoSearchResultsInterface;
use Hiberus\ExamenGuajardo\Api\AlumnoRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class MyPluginExam
 * @package Hiberus\ExamenGuajardo\Plugin
 */
class MyPluginExam
{
    

    /**
     * @param AlumnoRepositoryInterface $subject
     * @param AlumnoSearchResultsInterface $result
     * @return AlumnoSearchResultsInterface
     */
    public function afterGetList(
        AlumnoRepositoryInterface $subject,
        $result
    ) {
        /** @var AlumnoInterface $first */
        $misAlumnos = $result->getItems();
        foreach($misAlumnos as $alumno){
            if($alumno->getMark()<5){
                $alumno->setMark(4.9);
            }
        }
        return $result;
    }
}
